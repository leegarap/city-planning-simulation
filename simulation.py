import pygame
import numpy as np
from producer import *
from consumer import *
import matplotlib.pyplot as plt
class simulation:
    def __init__(self,W,H,W_GRID,H_GRID,N,isSimP=True):
        self.W=W
        self.H=H
        self.W_GRID=W_GRID
        self.H_GRID=H_GRID
        self.W_S=int(W/W_GRID)
        self.H_S=int(H/H_GRID)
        self.N=N
        self.running=False
        self.result=[]
        if(isSimP==True):
            self.P=Producer(W_GRID,H_GRID,N)
        else:
            self.P=Consumer(W_GRID,H_GRID,N)
    def run(self):
        pygame.init()
        pygame.display.set_caption("City Planning")
        f=open("./Result/MOVE/P/res.txt","w")
        f.write("STEP,Fitness\n")
        self.screen=pygame.display.set_mode((self.W,self.H))
        self.running=True
        step=1
        while(self.running):
            #print("STEP",step,":",end=' ')
            
            self.draw()
            pygame.image.save(self.screen,"./Result/MOVE/P/IMAGES/"+str(step)+'.png')
            self.update()
            self.evnt()
            fit=self.P.findFitness()
            #print(fit)
            f.write(str(step)+","+str(fit)+"\n")
            print(step,",",fit)
            
            #pygame.time.Clock().tick(60)
            step+=1
        f.close()
        pygame.quit()
    def evnt(self):
        for evnts in pygame.event.get():
            if(evnts.type==pygame.QUIT):
                self.running=False
    def update(self):
        self.P.replanning()
        #self.P.step_replanning()
    def draw(self):
        self.screen.fill((186, 207, 242))
        
        
        for i in range(self.H_GRID):
            for j in range(self.W_GRID):
                
                if(self.P.city[i,j]==1):
                    color=(255, 216, 230)
                elif(self.P.city[i,j]==7):
                    color=(244, 172, 65)
                elif(self.P.city[i,j]==14):
                    color=(199, 244, 65)
                elif(self.P.city[i,j]==21):
                    color=(84, 232, 0)
                elif(self.P.city[i,j]==28):
                    color=(0, 232, 212)
                elif(self.P.city[i,j]==35):
                    color=(0, 143, 232)
                elif(self.P.city[i,j]==42):
                    color=(88, 0, 232)
                elif(self.P.city[i,j]==49):
                    color=(204, 0, 232)
                elif(self.P.city[i,j]==56):
                    color=(232, 0, 162)
                elif(self.P.city[i,j]==63):
                    color=(232, 0, 104)
                else:
                    color=(0, 0, 0)
                
                pygame.draw.rect(self.screen,color,[j*self.W_S,i*self.H_S
                                                 ,j*self.W_S+self.W_S,i*self.H_S+self.H_S])
        for i in range(0,self.H,self.H_S):
            pygame.draw.line(self.screen,(0, 0, 0),(0,i),(self.W,i),1)
        for i in range(0,self.W,self.W_S):
            pygame.draw.line(self.screen,(0, 0,0),(i,0),(i,self.H),1)
        pygame.display.update()
                


















                
        