import numpy as np
import random
import math
class Consumer:
    def __init__(self,W,H,N):
        self.city = np.zeros([H,W])
        self.H=H
        self.W=W
        self.P=[1,7,14,21,28,35,42,49,56,63]
        self.P_ratio=[683,341,228,171,137,114,97,85,76,68]
        self.S_ratio=2000
        self.Pos=[]
        self.type_pos=[0]
        self.good_c_amount=[55,155,183,189,189,186,183,179,176,173]
        self.P_Population=[int(self.P_ratio[i]/self.S_ratio*N) for i in range( len(self.P_ratio) )]
        self.allow_to_move=None
        p=0
        for i in range(len(self.P)):
            for j in range(self.P_Population[i]):
                x=random.randint(0,self.W-1)
                y=random.randint(0,self.H-1)
                while(self.city[y,x]!=0):
                    x=random.randint(0,self.W-1)
                    y=random.randint(0,self.H-1)

                self.city[y,x]=self.P[i]
                self.Pos.append([x,y,p])
                p+=1
            self.type_pos.append(len(self.Pos))
        self.fitness=0
    def __str__(self):
        out=""
        for i in range(self.H):
            for j in range(self.W):
                out+=str(self.city[i,j])+' '
            out+='\n'
        return out
    def replanning(self):
        P_set=self.Pos
        P_set=random.sample(P_set,len(P_set))
        setted=0
        for (x,y,p) in P_set:
            this_point=self.city[y,x]
            self.city[y,x]=0
            new_set=self.findBestPoints(x,y)

            self.city[new_set[1],new_set[0]]=this_point
            new_set.append(p)
            self.Pos[p]=tuple(new_set)
            setted+=1
            
            #print("Relocation: "+str(setted)+"/"+str(len(P_set)))
    def step_replanning(self):
        if(self.allow_to_move==None or len(self.allow_to_move)==0):
            self.allow_to_move=self.Pos
            self.allow_to_move=random.sample(self.allow_to_move,len(self.allow_to_move))
        (x,y,p)=self.allow_to_move[0]
        this_point=self.city[y,x]
        self.city[y,x]=0
        new_set=self.findBestPoints(x,y)
        self.Pos[p]=new_set
        self.city[new_set[1],new_set[0]]=this_point
        self.allow_to_move.pop(0)
    def findBestPoints(self,x,y):
        select=self.good_c_amount[int(self.city[y,x]/7)]
        min_dist_point = []
        rg=1
        while(select):
            start_x=max(0,x-rg)
            end_x=min(self.W-1,x+rg)
            start_y=max(0,y-rg)
            end_y=min(self.H-1,y+rg)
            
            
            for i in range(start_x,end_x+1):
                
                if(self.city[start_y,i]!=0 and select!=0):
                    min_dist_point.append((i,start_y))
                    select-=1
                
                if(self.city[end_y,i]!=0 and select!=0):
                    min_dist_point.append((i,end_y))
                    select-=1
                    
            for i in range(start_y,end_y+1):
                
                if(self.city[i,start_x]!=0 and select!=0):
                    min_dist_point.append((start_x,i))
                    select-=1
                    
                if( self.city[i,end_x]!=0 and select!=0):
                    min_dist_point.append((end_x,i))
                    select-=1
            rg+=1
        start_x=x
        end_x=x
        start_y=y
        end_y=y
        best_x,best_y=x,y
        min_dis=9999999999999999
        
        for i in range(len(min_dist_point)):
            start_x=min(start_x,min_dist_point[i][0])
            end_x=max(end_x,min_dist_point[i][0])
            start_y=min(start_y,min_dist_point[i][1])
            end_y=max(end_y,min_dist_point[i][1])
        #print(start_x," ",end_x)
        #print(start_y," ",end_y)
        for i in range(start_y,end_y+1):
            for j in range(start_x,end_x+1):
                if(self.city[i,j]!=0):
                    continue
                dis=0
                for k in range(len(min_dist_point)):
                    dis+=self.findDistant(min_dist_point[k][0],min_dist_point[k][1],j,i)
                if(dis<min_dis):
                    min_dis=dis
                    best_x,best_y=j,i
        #print("Best X,Y:",best_x," ",best_y)
        return [best_x,best_y]
        
    def findDistant(self,xs,ys,xd,yd):
        return (xs-xd)**2+(ys-yd)**2
    def findCentroid(self,list_of_point):
        av_x=0
        av_y=0
        for i in range(len(list_of_point)):
            av_x+=list_of_point[i][0]
            av_y+=list_of_point[i][1]
        av_x/=len(list_of_point)
        av_y/=len(list_of_point)
        return (int(av_x),int(av_y))
    def findFitness(self):
        total_f=0
        for (x,y,_) in self.Pos:
            select=self.good_c_amount[int(self.city[y,x]/7)]
            rg=1
            min_distant=0
            while(select):
                start_x=max(0,x-rg)
                end_x=min(self.W-1,x+rg)
                start_y=max(0,y-rg)
                end_y=min(self.H-1,y+rg)
            
            
                for i in range(start_x,end_x+1):
                
                    if(self.city[start_y,i]!=0 and select!=0):
                        min_distant+=self.findDistant(x,y,i,start_y)
                        select-=1
                
                    if(self.city[end_y,i]!=0 and select!=0):
                        min_distant+=self.findDistant(x,y,i,end_y)
                        select-=1
                    
                for i in range(start_y,end_y+1):
                
                    if(self.city[i,start_x]!=0 and select!=0):
                        min_distant+=self.findDistant(x,y,start_x,i)
                        select-=1
                    
                    if( self.city[i,end_x]!=0 and select!=0):
                        min_distant+=self.findDistant(x,y,end_x,i)
                        select-=1
                rg+=1
            
            total_f+=min_distant
        self.fitness=total_f
        return total_f
        
        
        
        
        
        
        
        